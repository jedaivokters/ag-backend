<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Booking extends Model {
    protected $table = 'booking';
    protected $primaryKey = 'booking_id';

    protected $casts = [
        'data' => 'array',
        'remarks' => 'array'
    ];
    protected $appends = ['has_many_room'];
    protected $fillable =[
        "version",
        "tariff_id",
        "agency_id",
        "m_booking_status_id",
        "booking_date",
        "check_in_date",
        "check_out_date",
        "number_nights",
        "number_pax",
        "inquiry_date",
        "booked_date",
        "confirmed_date",
        "finalized_date",
        "completed_date",
        "refused_date",
        "canceled_date",
        "booking_remarks",
        "booking_reminder_id",
        "hotel_id",
        "grand_total",
        "sub_total",
        "room_total",
        "tax_total",
        "option_total",
        "commission_total",
        "data",
        "remarks",
        "sales_person"
    ];


    public function hasManyBookingLog() {
        return $this->hasMany('App\BookingLog','booking_id');
    }

    public function hasOneBookingOption() {

        return $this->hasOne('App\BookingOption', 'booking_id');
    }

    public function hasOneBookingOther() {

        return $this->hasOne('App\BookingOther', 'booking_id');
    }

    public function hasOneBookingInternal() {

        return $this->hasOne('App\BookingInternal', 'booking_id');
    }



    public function hasOneBookingMemo() {

        return $this->hasOne('App\BookingMemo', 'booking_id');
    }



    public function hasOneBookingTax() {

        return $this->hasOne('App\BookingTax', 'booking_id');
    }


    public function hasManyBookingTax() {

        return $this->hasMany('App\BookingTax', 'booking_id');
    }



    public function hasManyBookingRoom() {
        return $this->hasMany('App\BookingRoom', 'booking_id');
    }


    public function hasManyBookingReminder() {
        return $this->hasMany('App\BookingReminder','booking_id');
    }





    public function tariff() {
        return $this->belongsTo('App\Tariff','tariff_id');
    }

    public function hotel() {
        return $this->belongsTo('App\Hotel','hotel_id');
    }

    public function agency() {
        return $this->belongsTo('App\Agency','agency_id');
    }

    public function mBookingStatus() {
        return $this->belongsTo('App\MBookingStatus', 'm_booking_status_id');
    }


    public function getHasManyRoomAttribute() {


        return [];
    }

}
