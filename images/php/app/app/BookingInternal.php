<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class BookingInternal extends Model {


    protected $fillable =[

        "category",
        "type",
        "stage",
        "allotment",
        "tentative",
        "payment",
        "name",
        "note",
        "booking_id",
        "reservation_number",
    ];
}
