<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class BookingLog extends Model {


    protected $fillable = [
        "booking_id",
        "edited_by",
        "details",
        "m_booking_status_id"
    ];

    public function mBookingStatus() {
        return $this->belongsTo('App\MBookingStatus', 'm_booking_status_id');
    }
}
