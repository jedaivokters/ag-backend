<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class BookingMemo extends Model {
    protected $table = 'booking_memo';
    protected $primaryKey = 'booking_memo_id';

    protected $fillable = [
        "booking_id",
        "category",
        "memo_type",
        "probability",
        "payment",
        "namelist",
        "note",
        "booking_probability_type_id",
    ];



}
