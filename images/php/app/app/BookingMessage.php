<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class BookingMessage extends Model {


    protected $fillable =[
        "table_type",
        "table_id",
        "booking_id",
        "message",
        "has_file",
        "file_name",
        "path",
        "original_name",
    ];

}
