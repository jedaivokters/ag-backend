<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class BookingOption extends Model {
	protected $table = 'booking_option';
	protected $primaryKey = 'booking_option_id';

    protected $casts = [
        'children_no_beds' => 'array',
//        'driver_guides' => 'array',
        'meals' => 'array',
        'others' => 'array',
        'commissions' => 'array',

    ];


    protected $fillable = [
        'children_no_beds',
        'commissions',
        'booking_id',
        'meals',
        'tariff_id',
        'others',
    ];


}
