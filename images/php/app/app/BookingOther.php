<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class BookingOther extends Model {
    protected $table = 'booking_other';
    protected $primaryKey = 'booking_other_id';

    protected $fillable = [
        "booking_other_id",
        "booking_id",
        "nationality",
        "tour_code",
        "group_name",
        "transportation",
        "arrival_time",
        "departure_time",
        "message_hotel",
        "created_at",
        "updated_at",
        "contact_conductor_number",
        "contact_conductor_name"
    ];

    public function hasOneBookingOther() {
        return $this->belongsTo('App\BookingOther','booking_other_id');
    }
}
