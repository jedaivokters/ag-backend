<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class BookingRoom extends Model {
    protected $table = 'booking_room';
    protected $primaryKey = 'booking_room_id';

    protected $fillable = [

        "date",
        "room_id",
        "name",
        "number",
        "price",
        "total",
        "vat",
        "quantity",
        "night",
        "booking_id",
        "vat_type",
        "status"
    ];


    public function booking() {
        return $this->belongsTo('App\Booking','booking_id');
    }
}
