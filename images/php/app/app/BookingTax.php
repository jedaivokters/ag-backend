<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class BookingTax extends Model {

    protected $table = 'booking_tax';
    protected $primaryKey = 'booking_tax_id';
    protected $fillable = [
        "booking_id",
        "name",
        "quantity",

        "night",
        "price",
        "total",
        "created"
    ];
}
