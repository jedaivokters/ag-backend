<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class CalendarDetail extends Model {


	protected $fillable = [

		"calendar_header_id",
        "days",
        "month"
    ];
    protected $casts = [
        'days' => 'array',

    ];


	public function calendarHeader() {
	    return $this->belongsTo('App\CalendarHeader');
	}


}
