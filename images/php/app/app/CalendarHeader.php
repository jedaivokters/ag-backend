<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class CalendarHeader extends Model {


	protected $fillable = [
		"hotel_id",
		"year",
    ];

	public function hasManyCalendarDetail() {
	    return $this->hasMany('App\CalendarDetail');
	}
}
