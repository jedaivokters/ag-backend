<?php

namespace App\Emails;

class AgencyReceivedBookingEmail extends Email
{
    /**
     * Get the email id.
     *
     * @return string
     */
    public function getEmailId()
    {
        return 'f5438f79-09f6-40f4-ad2f-19857816df67';
    }
}