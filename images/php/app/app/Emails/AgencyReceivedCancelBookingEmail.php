<?php

namespace App\Emails;

class AgencyReceivedCancelBookingEmail extends Email
{
    /**
     * Get the email id.
     *
     * @return string
     */
    public function getEmailId()
    {
        return '196b4b52-ddd7-4ecb-904d-c38ce40a94a5';
    }
}