<?php

namespace App\Emails;

class AgencyReceivedRefuseBookingEmail extends Email
{
    /**
     * Get the email id.
     *
     * @return string
     */
    public function getEmailId()
    {
        return '620a4d26-396f-4add-b69f-5ebd92a98ea9';
    }
}