<?php

namespace App\Emails;

class AgencyRegistrationEmail extends Email
{
    /**
     * Get the email id.
     *
     * @return string
     */
    public function getEmailId()
    {
        return '1e3bc83a-6a69-466c-9ecb-807af558330d';
    }
}