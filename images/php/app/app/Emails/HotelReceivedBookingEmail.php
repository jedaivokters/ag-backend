<?php

namespace App\Emails;

class HotelReceivedBookingEmail extends Email
{
    /**
     * Get the email id.
     *
     * @return string
     */
    public function getEmailId()
    {
        return 'a2b2a7aa-2878-4eeb-8387-1dbc7ce12ad2';
    }
}