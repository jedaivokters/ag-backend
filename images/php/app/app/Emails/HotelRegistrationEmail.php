<?php

namespace App\Emails;

class HotelRegistrationEmail extends Email
{
    /**
     * Get the email id.
     *
     * @return string
     */
    public function getEmailId()
    {
        return '2cd3fcfc-7dc2-46f1-81ae-d5a6bd76bc20';
    }
}