<?php

namespace App\Emails;

class HotelUpdateEmail extends Email
{
    /**
     * Get the email id.
     *
     * @return string
     */
    public function getEmailId()
    {
        return '76d5c547-d380-4325-b247-ee720ed8c5af';
    }
}