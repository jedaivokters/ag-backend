<?php

namespace App\Emails;

class InquiryEmail extends Email
{
    /**
     * Get the email id.
     *
     * @return string
     */
    public function getEmailId()
    {
        return '0a2f5fce-518a-4b5e-b799-92342f9ceaac';
    }
}