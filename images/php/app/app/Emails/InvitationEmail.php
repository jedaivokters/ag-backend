<?php

namespace App\Emails;

class InvitationEmail extends Email
{
    /**
     * Get the email id.
     *
     * @return string
     */
    public function getEmailId()
    {
        return '63a2bad7-8819-4a23-8559-8d7a84ee41ad';
    }
}