<?php

namespace App\Emails;

class VerificationEmail extends Email
{
    /**
     * Get the email id.
     *
     * @return string
     */
    public function getEmailId()
    {
        return 'ad012d41-6d58-40b1-b7fb-d1a2aea5874d';
    }
}