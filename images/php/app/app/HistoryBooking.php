<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class HistoryBooking extends Model {
    protected $casts = [

        'remarks' => 'array'
    ];
	protected $fillable =[
		"booking_id",
		"version",
		"tariff_id",
		"agency_id",
		"m_booking_status_id",
		"booking_date",
		"check_in_date",
		"check_out_date",
		"number_nights",
		"number_pax",
		"inquiry_date",
		"booked_date",
		"confirmed_date",
		"finalized_date",
		"booking_remarks",
		"booking_reminder_id",
		"created_at",
		"updated_at",
		"hotel_id",
        "sub_total",
        "room_total",
        "tax_total",
        "option_total",
        "commission_total",
        "grand_total",
        "remarks"
	];


    public function bookingLog() {
        return $this->belongsTo('App\BookingLog','booking_log_id');
    }

    public function hasOneHistoryBookingMemo() {

        return $this->hasOne('App\HistoryBookingMemo','history_booking_id');
    }

    public function hasOneHistoryBookingOther() {

        return $this->hasOne('App\HistoryBookingOther','history_booking_id');
    }
    public function hasOneHistoryBookingOption() {

        return $this->hasOne('App\HistoryBookingOption','history_booking_id');
    }

    public function hasOneHistoryBookingInternal() {

        return $this->hasOne('App\HistoryBookingInternal', 'history_booking_id');
    }


    public function hasManyHistoryBookingRoom() {
        return $this->hasMany('App\HistoryBookingRoom','history_booking_id');
    }
    public function hasManyHistoryBookingTax() {
        return $this->hasMany('App\HistoryBookingTax','history_booking_id');
    }
	public function tariff() {
		return $this->belongsTo('App\Tariff','tariff_id');
	}

	public function hotel() {
		return $this->belongsTo('App\Hotel','hotel_id');
	}

	public function agency() {
		return $this->belongsTo('App\Agency','agency_id');
	}

	public function mBookingStatus() {
		return $this->belongsTo('App\MBookingStatus', 'm_booking_status_id');
	}

	public function deleteRelatedData() {
		$this->hasOneHistoryBookingMemo()->delete();
		$this->hasOneHistoryBookingOther()->delete();
		$this->hasOneHistoryBookingOption()->delete();
		$this->hasManyHistoryBookingRoom()->delete();
	}

}
