<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class HistoryBookingInternal extends Model {


    protected $fillable =[
        "category",
        "type",
        "stage",
        "allotment",
        "tentative",
        "payment",
        "name",
        "note",
        "booking_id",
        "history_booking_id",
        "reservation_number",
    ];
}
