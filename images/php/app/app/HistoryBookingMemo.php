<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class HistoryBookingMemo extends Model {

	protected $fillable =[
		'booking_memo_id',
		'booking_id',
		'category',
		'memo_type',
		'probability',
		'payment',
		'namelist',
		'note',
		'booking_probability_type_id',
		'created_at',
		'updated_at',
	];
}
