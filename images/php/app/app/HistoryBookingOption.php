<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class HistoryBookingOption extends Model {

	protected $casts = [
		'children_no_beds' => 'array',
//        'driver_guides' => 'array',
		'meals' => 'array',
		'others' => 'array',
		'commissions' => 'array',

	];

	protected $fillable =[
		'booking_option_id',
		'booking_id',
		'meals',
		'others',
		'commissions',
		'children_no_beds',
		'driver_guides',
		'created_at',
		'updated_at'
	];
}
