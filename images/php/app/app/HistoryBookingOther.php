<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class HistoryBookingOther extends Model {

	protected $fillable =[
		'booking_other_id',
		'booking_id',
		'nationality',
		'tour_code',
		'group_name',
		'transportation',
		'arrival_time',
		'departure_time',
		'message_hotel',
		'created_at',
		'updated_at',
		'contact_conductor_number',
		'contact_conductor_name'
	];
}
