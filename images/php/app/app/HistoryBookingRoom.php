<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class HistoryBookingRoom extends Model {
	protected $fillable =[
		'booking_room_id',
		'price',
		'date',
		'room_id',
		'name',
		'number',
		'price',
		'total',
		'vat',
		'quantity',
		'night',
		'booking_id',
		'created_at',
		'updated_at'
	];
}
