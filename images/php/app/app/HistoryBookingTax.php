<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class HistoryBookingTax extends Model {


    protected $primaryKey = 'id';
    protected $fillable = [
        "booking_id",
        "name",
        "quantity",
        "booking_tax_id",
        "night",
        "price",
        "total",
        "history_booking_id",
        "created"
    ];
}
