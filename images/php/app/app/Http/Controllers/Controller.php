<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\Paginator;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Collection;

class Controller extends BaseController {
    //


    public function paginate($data, $perPage) {

        $collection = new Collection($data);

        $paginator = new Paginator($collection, $perPage);
        $response = collect(['total' => count($data)])->merge($paginator);

        return $response;
    }


    public function validator($data, $rules) {

        return \Validator::make($data, $rules);
    }


    protected function createdResponse($data) {
        $response = [
            'code'   => 201,
            'status' => 'succcess',
            'data'   => $data,
        ];

        return response()->json($response, $response['code']);
    }

    protected function showResponse($data) {
        $response = [
            'code'   => 200,
            'status' => 'succcess',
            'data'   => $data,
        ];

        return response()->json($response, $response['code']);
    }

    protected function listResponse($data) {
        $response = [
            'code'   => 200,
            'status' => 'succcess',
            'data'   => $data,
        ];

        return response()->json($response, $response['code']);
    }

    protected function notFoundResponse() {
        $response = [
            'code'    => 404,
            'status'  => 'error',
            'data'    => 'Resource Not Found',
            'message' => 'Not Found',
        ];

        return response()->json($response, $response['code']);
    }

    protected function deletedResponse() {
        $response = [
            'code'    => 204,
            'status'  => 'success',
            'data'    => [],
            'message' => 'Resource deleted',
        ];

        return response()->json($response, $response['code']);
    }

    protected function clientErrorResponse($data) {
        $response = [
            'code'    => 422,
            'status'  => 'error',
            'data'    => $data,
            'message' => 'Unprocessable entity',
        ];

        return response()->json($response, $response['code']);
    }
}
