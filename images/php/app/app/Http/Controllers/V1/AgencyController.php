<?php

namespace App\Http\Controllers\V1;


use App\Agency;
use App\Booking;
use App\Http\Controllers\Controller;
use App\Repositories\Rest\RestRepository;
use Illuminate\Http\Request;

use App\Emails\AgencyRegistrationEmail;

class AgencyController extends Controller {


    protected $rules = [
        "name"        => "required",
        "postal_code" => "required",
        "prefecture"  => "required",
        "city"        => "required",
        "phone"       => "required",
    ];
    /**
     * @var RestRepository
     */
    private $rest;
    /**
     * @var Booking
     */
    private $booking;

    public function __construct(Agency $rest, Booking $booking) {

        $this->rest = new RestRepository($rest);
        $this->booking = $booking;
    }


    public function hotelCustomerOnly($request) {

        $response = [];
        $data = $this->booking->whereHotelId($request->user()->platform_id)->groupBy('agency_id')->get();

        foreach ($data as $key => $res) {

            array_push($response, $res->agency_id);
        }

        return $response;
    }

    public function index(Request $request) {
        $data = $request->all();


        $response = $this->rest->getModel();

        if (isset($data['customer_only']) && $data['customer_only'] && json_decode($data['customer_only'])) {

            $agencies = $this->hotelCustomerOnly($request);
            $response = $response->whereIn('agency_id', $agencies);
        }

        $response = json_decode($data['pagination']) ? $response->paginate(10) : $response = $response->get();

        return $this->listResponse($response);

    }

    public function show($id) {
        if ($response = $this->rest->getModel()->find($id)) {
            return $this->showResponse($response);
        }

        return $this->notFoundResponse();
    }

    public function getPlatformByUser(Request $request) {


        $response = $this->rest->getModel()->with(['hasOneUserAccess'])
            ->whereHas('hasOneUserAccess', function ($query) use ($request) {
                $query->whereUserId($request->user()->id);
            })
            ->first();


        if ($response) {
            return $this->showResponse($response);
        }

        return $this->notFoundResponse();
    }

    public function store(Request $request) {
        $data = $request->all();

        try {
            $validator = $this->validator($data, $this->rules);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            } else {
                $response = $this->rest->create($data);

                //TODO: Email status send to logger. Should be on the Email base class
                $registrationEmail = new AgencyRegistrationEmail();
                $registrationEmail
                    ->withData(['name' => $data['name']])
                    ->sendTo(
                        ['email' => $data['account']],
                        ['yes', false]
                    );

                return $this->createdResponse($response);
            }
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }
    }

    public function update($id, Request $request) {

        $data = $request->all();
        if ($response = $this->rest->getModel()->find($id)) {

            try {
                $validator = $this->validator($data, $this->rules);

                if ($validator->fails()) {
                    throw new \Exception("ValidationException");
                }
                $response->fill($data)->save();


                return $this->showResponse($response);
            } catch (\Exception $ex) {
                $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

                return $this->clientErrorResponse($response);
            }
        }

        return $this->notFoundResponse();

    }

    public function destroy($id) {


        if ($response = $this->rest->getModel()->find($id)) {

            $response->delete();

            return $this->deletedResponse();

        }

        return $this->notFoundResponse();

    }
}
