<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\User;
use App\UserAccess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\JWTAuth;

use App\Emails\VerificationEmail;


class AuthController extends Controller {
	/**
	 * @var JWTAuth
	 */
	private $jwtAuth;
	/**
	 * @var Request
	 */
	private $request;
	private $guard;
	/**
	 * @var UserAccess
	 */
	private $userAccess;
    /**
     * @var User
     */
    private $user;

    /**
	 * @var \Tymon\JWTAuth\JWTAuth
	 */

	public function __construct(JWTAuth $jwtAuth, UserAccess $userAccess, User $user) {

//		$this->middleware('auth:api', ['except' => ['login']]);
		$this->jwtAuth = $jwtAuth;


		$this->userAccess = $userAccess;
        $this->user = $user;
    }

    /*
    	Email test: please remove this in the future
    */
    public function test() {
    	$auth = array('api_key' => 'wi9STkxMC14cB2OoVvN0ym21X424I+rMfuqVmtnae33ifcOOeflEDFCOMKjsqfnhjHjXUt8cG89EGNaqd4jcJytE/NvBtBTEFGoxO9f5shooRYJ4jgkGeB7eOsXX85oGSX8gSWAHaidjfCYVxdwyfg==');
		# The unique identifier for this smart email
    	$smart_email_id = 'ad012d41-6d58-40b1-b7fb-d1a2aea5874d';

		# Create a new mailer and define your message
    	$wrap = new \CS_REST_Transactional_SmartEmail($smart_email_id, $auth);
    	$message = array(
    		"To" => 'Allan Bernabe <allan.bernabe@gmail.com>',
    		"Data" => array(
    			'x-apple-data-detectors' => 'x-apple-data-detectorsTestValue',
    			'href^="tel"' => 'href^="tel"TestValue',
    			'href^="sms"' => 'href^="sms"TestValue',
    			'owa' => 'owaTestValue',
    			'role=section' => 'role=sectionTestValue',
    			'style*="font-size:1px"' => 'style*="font-size:1px"TestValue',
    			'url' => '<a href="google.com">link here</a>',
    		)
    	);

		# Add consent to track value
		$consent_to_track = 'no'; # Valid: 'yes', 'no', 'unchanged'

		# Send the message and save the response
		$result = $wrap->send($message, $consent_to_track, false);

		echo '<pre>';
		var_dump($result->response);
    }

	public function login(Request $request) {
		
		$this->validate($request, [
			'email'    => 'required|max:255',
			'password' => 'required',
		]);

		$credential = $request->only('email', 'password');
		$token = $this->jwtAuth->attempt($credential);
		if (!$token) {
			$error = [

				"user" => ["user not found"],
			];
			$response = ['form_validations' => $error, 'exception' => 'NotFoundException'];

            return $this->clientErrorResponse($response);

		}

		return $this->respondWithToken($token);

	}

	public function profile() {

		$inputs = [
			'status'  => true,
			'message' => 'success',
			'code'    => 200,
			'data'    => $this->jwtAuth->parseToken()->toUser(),
		];


		return response()->json($inputs, 200);
	}

    public function changePassword(Request $request) {
        $data = $request->all();

        $rules = [
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        ];

        $id =  $this->jwtAuth->parseToken()->toUser()->id;

        try {
            $validator = $this->validator($data,$rules);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }
            $response = $this->user->find($id);
            $response->fill($data)->save();



            return $this->createdResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }



    }

	public function logout() {
		$this->jwtAuth->parseToken()->invalidate();

		$inputs = [
			'status'  => true,
			'message' => 'Successfully logged out',
			'code'    => 200,
			'data'    => [],
		];

		return response()->json($inputs, 200);
	}

	protected function respondWithToken($token) {

		$user = $this->jwtAuth->user();
		$access =  $this->userAccess->whereUserId($user->id)->first();

		$inputs = [
				'access_token' => $token,
				'user_id' => $user->id,
				'type' => $access->type

		];

		return $this->showResponse($inputs);
	}

}
