<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\Traits\RestTrait;


class BedTypeController extends Controller {
	use RestTrait;
	const MODEL = 'App\BedType';
	protected $validationRules = [
		"name" => "required",

	];


}
