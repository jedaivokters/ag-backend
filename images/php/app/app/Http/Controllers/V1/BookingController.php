<?php

namespace App\Http\Controllers\V1;


use App\Booking;
use App\BookingOther;
use App\MBookingStatus;
use App\Http\Controllers\Controller;
use App\Repositories\Rest\RestRepository;
use Illuminate\Http\Request;

use App\Emails\AgencyReceivedBookingEmail;
use App\Emails\HotelReceivedBookingEmail;
use App\Emails\AgencyReceivedCancelBookingEmail;
use App\Emails\AgencyReceivedRefuseBookingEmail;
use Illuminate\Support\Facades\DB;


class BookingController extends Controller {


    protected $rules = [];
    /**
     * @var RestRepository
     */
    private $rest;

    public function __construct(Booking $rest) {

        $this->rest = new RestRepository($rest);
    }


    public function index(Request $request) {
        $data = $request->all();


        try {
            $validator = $this->validator($data, ['pagination' => 'required', 'platform' => 'required', 'order_by' => 'required', 'order' => 'required']);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }

            $response = $this->rest->getModel()->with(['mBookingStatus', 'agency', 'hotel', 'hasOneBookingOther', 'hasOneBookingInternal']);

            if (isset($data['m_booking_status_id']) && $data['m_booking_status_id'] && $data['order_by'] != 'nationality') {
                $response = $response->whereIn('m_booking_status_id', explode(',', $data['m_booking_status_id']))->orderBy($data['order_by'], $data['order']);

            } else {


                if ($data['order_by'] == 'm_booking_status_id') {

                    $response = $response->orderByRaw(
                        DB::raw('CASE
					WHEN m_booking_status_id = 7 THEN 7
					WHEN m_booking_status_id = 1 THEN 6
					WHEN m_booking_status_id = 2 THEN 5
					WHEN m_booking_status_id = 4 THEN 4
					WHEN m_booking_status_id = 3 THEN 3
					WHEN m_booking_status_id = 5 THEN 2
					WHEN m_booking_status_id = 6 THEN 1
				
					END ' . $data['order']
                        )
                    );
                }
            }
            if (isset($data['m_booking_status_id']) && !$data['m_booking_status_id']) {
                if ($data['order_by'] == 'nationality') {
                    if ($data['order_by'] == 'nationality') {
                        $response = $response->orderBy($data['order_by'], $data['order']);
                        $response = $response->join('booking_other', 'booking.booking_id', '=', 'booking_other.booking_id')
                            ->orderBy('booking_other.nationality', $data['order'])
                            ->select('booking.*');
                    }
                }
                else{
                    $response = $response->orderBy($data['order_by'], $data['order']);
                }

            } else if (isset($data['m_booking_status_id']) && $data['m_booking_status_id'] && $data['order_by'] == 'nationality') {

                $response = $response->whereIn('m_booking_status_id', explode(',', $data['m_booking_status_id']));
                $response = $response->join('booking_other', 'booking.booking_id', '=', 'booking_other.booking_id')
                    ->orderBy('booking_other.nationality', $data['order'])
                    ->select('booking.*');

            } else {

                if ($data['order_by'] == 'nationality') {
                    $response = $response->orderBy($data['order_by'], $data['order']);
                    $response = $response->join('booking_other', 'booking.booking_id', '=', 'booking_other.booking_id')
                        ->orderBy('booking_other.nationality', $data['order'])
                        ->select('booking.*');
                }

            }
            if (isset($data['within_days']) && $data['within_days'] != '') {
                $dateNow = date('Y-m-d');
                $dateNow = date('Y-m-d', strtotime($dateNow));
                $dateAfter = date('Y-m-d', strtotime($dateNow . ' + ' . $data['within_days'] . ' days'));
                $response = $response->where('check_in_date', '>=', $dateNow)->where('check_in_date', '<=', $dateAfter);
            }
            if (isset($data['check_in_date']) && $data['check_in_date']) {
                $response = $response->whereDate('check_in_date', $data['check_in_date']);
            }
            if (isset($data['check_out_date']) && $data['check_out_date']) {
                $response = $response->whereDate('check_out_date', $data['check_out_date']);
            }
            if (isset($data['number_pax']) && $data['number_pax']) {
                $response = $response->where('number_pax', $data['number_pax']);
            }
            if (isset($data['agency_id']) && $data['agency_id']) {
                $response = $response->where('agency_id', $data['agency_id']);
            }

            if (isset($data['hotel_id']) && $data['hotel_id']) {
                $response = $response->where('hotel_id', $data['hotel_id']);
            }
            $response = $response->where($data['platform'], $request->user()->platform_id);

            $response = json_decode($data['pagination']) ? $response->paginate($data['per_page']) : $response = $response->get();

            return $this->listResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }


        return $this->listResponse($response);


    }

    public function show($id) {
        $response = $this->rest->getModel()->with([
            'tariff',
            'hotel',
            'mBookingStatus',
            'agency',
            'hasOneBookingOption',
            'hasOneBookingTax',
            'hasManyBookingTax',
            'hasManyBookingLog' => function ($query) {
                $query->with(['mBookingStatus']);
            },
            'hasOneBookingOther',
            'hasOneBookingMemo',
            'hasManyBookingRoom',
            'hasOneBookingInternal',
        ])->find($id);


        if ($response) {
            return $this->showResponse($response);
        }

        return $this->notFoundResponse();
    }

    public function store(Request $request) {
        $data = $request->all();
        $data['data'] = $data;

        try {
            $validator = $this->validator($data, $this->rules);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }
            $response = $this->rest->create($data);

            $booking_id = $response['booking_id'];

            $this->bookingRequestEmail($booking_id);

            return $this->createdResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }
    }

    private function bookingRequestEmail($booking_id) {

        $booking = $this->rest->getModel()->with([
            'hotel',
            'agency',
            'mBookingStatus',
        ])->find($booking_id);

        $url = env('FRONTEND_URL', 'http://localhost')
            . '/agency/reservations/' . $booking_id . '/details';

        //Email send to OP
        $agency_data = [
            'op_name'            => $booking['agency']['name'],
            'check_in'           => $booking['check_in_date'],
            'check_out'          => $booking['check_out_date'],
            'nights'             => $booking['number_nights'],
            'pax'                => $booking['number_pax'],
            'url'                => $url,
            'sp_name'            => $booking['hotel']['name'],
            'reservation_status' => $booking['mBookingStatus']['name'],
        ];

        $agency_booking = new AgencyReceivedBookingEmail();
        $agency_booking
            ->withData($agency_data)
            ->sendTo(
                ['email' => $booking['agency']['account']],
                ['yes', false]
            );

        //Email send to SP
        $url = env('FRONTEND_URL', 'http://localhost')
            . '/hotel/reservations/' . $booking_id . '/details';
        $op_address = sprintf("%s %s %s %s",
            $booking['agency']['street'],
            $booking['agency']['prefecture'],
            $booking['agency']['city'],
            $booking['agency']['postal_code']);

        $hotel_data = [
            'op_name'            => $booking['agency']['name'],
            'op_prefecture'      => $booking['agency']['prefecture'],
            'op_city'            => $booking['agency']['city'],
            'op_address'         => $op_address,
            'op_phone'           => $booking['agency']['phone'],
            'op_url'             => $booking['agency']['url'],
            'op_email'           => $booking['agency']['account'],
            'check_in'           => $booking['check_in_date'],
            'check_out'          => $booking['check_out_date'],
            'nights'             => $booking['number_nights'],
            'pax'                => $booking['number_pax'],
            'url'                => $url,
            'sp_name'            => $booking['hotel']['name'],
            'reservation_status' => $booking['mBookingStatus']['name'],
        ];

        $hotel_booking = new HotelReceivedBookingEmail();
        $hotel_booking
            ->withData($hotel_data)
            ->sendTo(
                ['email' => $booking['hotel']['account']],
                ['yes', false]
            );

    }

    public function update($id, Request $request) {

        $data = $request->all();

        if ($response = $this->rest->getModel()->find($id)) {

            try {
                $validator = $this->validator($data, $this->rules);

                if ($validator->fails()) {
                    throw new \Exception("ValidationException");
                }

                $response->fill($data)->save();

                if ($data['m_booking_status_id'] == 6) { //Cancel status

                    $booking_id = $data['booking_id'];
                    $booking_status = MBookingStatus::find($data['m_booking_status_id'])->name;

                    $url = env('FRONTEND_URL', 'http://localhost')
                        . '/agency/reservations/' . $booking_id . '/details';

                    //Email send to OP
                    $agency_data = [
                        'op_name'            => $data['agency']['name'],
                        'group_name'         => $data['has_one_booking_other']['group_name'],
                        'check_in'           => $data['check_in_date'],
                        'check_out'          => $data['check_out_date'],
                        'nights'             => $data['number_nights'],
                        'pax'                => $data['number_pax'],
                        'url'                => $url,
                        'sp_name'            => $data['hotel']['name'],
                        'reservation_status' => $booking_status,
                    ];

                    $agency_booking = new AgencyReceivedCancelBookingEmail();
                    $agency_booking
                        ->withData($agency_data)
                        ->sendTo(
                            ['email' => $data['agency']['account']],
                            ['yes', false]
                        );

                }

                if ($data['m_booking_status_id'] == 7) { //Refuse status

                    $booking_id = $data['booking_id'];
                    $booking_status = MBookingStatus::find($data['m_booking_status_id'])->name;

                    $url = env('FRONTEND_URL', 'http://localhost')
                        . '/agency/reservations/' . $booking_id . '/details';

                    //Email send to OP
                    $agency_data = [
                        'op_name'            => $data['agency']['name'],
                        'group_name'         => $data['has_one_booking_other']['group_name'],
                        'check_in'           => $data['check_in_date'],
                        'check_out'          => $data['check_out_date'],
                        'nights'             => $data['number_nights'],
                        'pax'                => $data['number_pax'],
                        'url'                => $url,
                        'sp_name'            => $data['hotel']['name'],
                        'reservation_status' => $booking_status,
                    ];

                    $agency_booking = new AgencyReceivedRefuseBookingEmail();
                    $agency_booking
                        ->withData($agency_data)
                        ->sendTo(
                            ['email' => $data['agency']['account']],
                            ['yes', false]
                        );

                }

                return $this->showResponse($response);
            } catch (\Exception $ex) {
                $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

                return $this->clientErrorResponse($response);
            }
        }

        return $this->notFoundResponse();

    }

    public function destroy($id) {


        if ($response = $this->rest->getModel()->find($id)) {

            $response->delete();

            return $this->deletedResponse();

        }

        return $this->notFoundResponse();

    }
}
