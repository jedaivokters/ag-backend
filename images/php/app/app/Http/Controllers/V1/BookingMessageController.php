<?php

namespace App\Http\Controllers\V1;


use App\BookingMessage;
use App\Http\Controllers\Controller;
use App\Repositories\Rest\RestRepository;
use Illuminate\Http\Request;


class BookingMessageController extends Controller {


    protected $rules = [];
    /**
     * @var RestRepository
     */
    private $rest;

    public function __construct(BookingMessage $rest) {

        $this->rest = new RestRepository($rest);
    }


    public function index(Request $request) {

        $data = $request->all();


        try {
            $validator = $this->validator($data, ['pagination' => 'required','booking_id' => 'required']);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }
//            $response = $this->rest->getModel()->whereHotelId($request->user()->platform_id);
            $response = $this->rest->getModel()->orderBy('id','DESC')->whereBookingId($data['booking_id']);


            $response = json_decode($data['pagination']) ? $response->paginate(10) : $response = $response->get();

            return $this->listResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }


        return $this->listResponse($response);


    }

    public function show($id) {
        if ($response = $this->rest->getModel()->find($id)) {
            return $this->showResponse($response);
        }

        return $this->notFoundResponse();
    }

    public function store(Request $request) {
        $data = $request->all();


        try {
            $validator = $this->validator($data, $this->rules);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }
            $response = $this->rest->create($data);

            return $this->createdResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }
    }

    public function update($id, Request $request) {

        $data = $request->all();
        if ($response = $this->rest->getModel()->find($id)) {

            try {
                $validator = $this->validator($data, $this->rules);

                if ($validator->fails()) {
                    throw new \Exception("ValidationException");
                }
                $response->fill($data)->save();

                return $this->showResponse($response);
            } catch (\Exception $ex) {
                $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

                return $this->clientErrorResponse($response);
            }
        }

        return $this->notFoundResponse();

    }

    public function destroy($id) {


        if ($response = $this->rest->getModel()->find($id)) {

            $response->delete();

            return $this->deletedResponse();

        }

        return $this->notFoundResponse();

    }
}
