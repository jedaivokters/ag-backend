<?php

namespace App\Http\Controllers\V1;


use App\BookingRoom;
use App\Http\Controllers\Controller;
use App\Repositories\Rest\RestRepository;
use Illuminate\Http\Request;


class BookingRoomController extends Controller {


    protected $rules = [];
    /**
     * @var RestRepository
     */
    private $rest;
    /**
     * @var BookingRoom
     */
    private $bookingRoom;

    public function __construct(BookingRoom $rest) {

        $this->rest = new RestRepository($rest);
        $this->bookingRoom = $rest;
    }


    public function index(Request $request) {

        $data = $request->all();


        try {
            $validator = $this->validator($data, ['pagination' => 'required']);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }
//            $response = $this->rest->getModel()->whereHotelId($request->user()->platform_id);
            $response = $this->rest->getModel();

            $response = json_decode($data['pagination']) ? $response->paginate(10) : $response = $response->get();

            return $this->listResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }


        return $this->listResponse($response);


    }

    public function show($id) {
        if ($response = $this->rest->getModel()->find($id)) {
            return $this->showResponse($response);
        }

        return $this->notFoundResponse();
    }

    public function store(Request $request) {
        $data = $request->all();




        try {
            $validator = $this->validator($data, $this->rules);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }

            foreach ($data['items'] as $key => $value){
                $value['booking_id'] = $data['booking_id'];
                $response = $this->rest->create($value);
            }


            return $this->createdResponse($data);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }
    }

    public function update($id, Request $request) {

        $data = $request->all();
        if ($response = $this->rest->getModel()->whereBookingId($id)->get()) {

            try {
                $validator = $this->validator($data, []);

                if ($validator->fails()) {
                    throw new \Exception("ValidationException");
                }
                foreach ($response as $key => $value){
                    $value->delete();
                }

                foreach ($data['items'] as $key => $value ) {


                    $this->bookingRoom->updateOrCreate(
                        ['booking_id' => $id, 'booking_room_id' => $value['booking_room_id']],
                        $value
                    );
                }


                return $this->showResponse($data);
            } catch (\Exception $ex) {
                $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

                return $this->clientErrorResponse($response);
            }
        }

        return $this->notFoundResponse();

    }

    public function destroy($id) {


        if ($response = $this->rest->getModel()->find($id)) {

            $response->delete();

            return $this->deletedResponse();

        }

        return $this->notFoundResponse();

    }
}
