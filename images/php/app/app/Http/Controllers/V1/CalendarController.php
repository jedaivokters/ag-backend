<?php

namespace App\Http\Controllers\V1;

use App\CalendarDetail;
use App\CalendarHeader;
use App\Http\Controllers\Controller;
use App\Repositories\Rest\RestRepository;
use Illuminate\Http\Request;


class CalendarController extends Controller {


    protected $rules = [
        "hotel_id" => "required",
    ];
    /**
     * @var RestRepository
     */
    private $rest;
    /**
     * @var CalendarDetail
     */
    private $calendarDetail;

    public function __construct(CalendarHeader $rest, CalendarDetail $calendarDetail) {

        $this->rest = new RestRepository($rest);


        $this->calendarDetail = $calendarDetail;
    }


    public function index(Request $request) {

        $data = $request->all();


        try {
            $validator = $this->validator($data, ['pagination' => 'required']);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }
//            $response = $this->rest->getModel()->whereHotelId($request->user()->platform_id);
            $response = $this->rest->getModel();


            $response = json_decode($data['pagination']) ? $response->paginate(10) : $response = $response->get();

            return $this->listResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }


        return $this->listResponse($response);


    }

    public function show($id, Request $request) {
        $data = $request->all();

        if ($response = $this->rest->getModel()->with(['hasManyCalendarDetail'])->whereHotelId($data['hotel_id'])->where('year', $id)->first()) {
            return $this->showResponse($response);
        }

        return $this->notFoundResponse();
    }

    public function store(Request $request) {
        $data = $request->all();

        try {
            $validator = $this->validator($data, $this->rules);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }

//            $response = $this->rest->create($data);

            $response = $this->rest->getModel()->updateOrCreate(
                ['hotel_id' => $data['hotel_id'], 'year' => $data['year']],
                ['hotel_id' => $data['hotel_id'], 'year' => $data['year']]
            );

            foreach ($data['has_many_calendar_detail'] as $key => $value) {
                $this->calendarDetail = $this->calendarDetail->updateOrCreate(
                    ['calendar_header_id' => $response->id, 'month' => $value['month']],
                    ['days' => $value['days'], 'calendar_header_id' => $response->id, 'month' => $value['month']]
                );
            }


            return $this->createdResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }
    }

    public function update($id, Request $request) {

        $data = $request->all();
        if ($response = $this->rest->getModel()->find($id)) {

            try {
                $validator = $this->validator($data, $this->rules);

                if ($validator->fails()) {
                    throw new \Exception("ValidationException");
                }
                $response->fill($data)->save();

                foreach ($data['has_many_calendar_detail'] as $key => $value) {
                    $this->calendarDetail = $this->calendarDetail->updateOrCreate(
                        ['calendar_header_id' => $id, 'month' => $value['month']],
                        ['days' => $value['days']]
                    );
                }

                return $this->showResponse($response);
            } catch (\Exception $ex) {
                $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

                return $this->clientErrorResponse($response);
            }
        }

        return $this->notFoundResponse();

    }

    public function destroy($id) {


        if ($response = $this->rest->getModel()->find($id)) {

            return $this->deletedResponse();

        }

        return $this->notFoundResponse();

    }
}
