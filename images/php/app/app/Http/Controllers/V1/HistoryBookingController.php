<?php

namespace App\Http\Controllers\V1;


use App\HistoryBooking;
use App\Http\Controllers\Controller;
use App\Repositories\Rest\RestRepository;
use Illuminate\Http\Request;


class HistoryBookingController extends Controller {


    protected $rules = [];
    /**
     * @var RestRepository
     */
    private $rest;

    public function __construct(HistoryBooking $rest) {

        $this->rest = new RestRepository($rest);
    }


    public function index(Request $request) {
        $data = $request->all();

        try {
            $validator = $this->validator($data, ['pagination' => 'required', 'platform' => 'required', 'order_by' => 'required', 'order' => 'required', 'page' => 'required', 'per_page' => 'required']);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }
            $response = $this->rest->getModel()->with(['mBookingStatus', 'hotel', 'hasOneHistoryBookingOther', 'agency']);
            if (isset($data['m_booking_status_id']) && $data['m_booking_status_id'] && $data['order_by'] != 'nationality') {
                $response = $response->whereIn('m_booking_status_id', explode(',', $data['m_booking_status_id']))->orderBy($data['order_by'], $data
                ['order']);


            }
            if (isset($data['within_days']) && $data['within_days'] != '') {
                $dateNow = date('Y-m-d');
                $dateNow = date('Y-m-d', strtotime($dateNow));
                $dateAfter = date('Y-m-d', strtotime($dateNow . ' + ' . $data['within_days'] . ' days'));
                $response = $response->where('check_in_date', '>=', $dateNow)->where('check_in_date', '<=', $dateAfter);
            }


            // if(isset($data['booking_id']) && $data['booking_id'] != ''){
            //     $response = $response->where('booking_id', $data['booking_id']);

            // }

            if (isset($data['m_booking_status_id']) && $data['m_booking_status_id'] && $data['order_by'] == 'nationality') {
                $response = $response->whereIn('m_booking_status_id', explode(',', $data['m_booking_status_id']));
                if (isset($data['booking_id']) && $data['booking_id'] != '') {
                    $response = $response->join('history_booking_others', 'history_bookings.id', '=', 'history_booking_others.history_booking_id')
                        ->orderBy('history_booking_others.nationality', $data['order'])
                        ->where('history_booking_others.booking_id', $data['booking_id'])
                        ->select('history_bookings.*');
                } else {
                    $response = $response->join('history_booking_others', 'history_bookings.id', '=', 'history_booking_others.history_booking_id')
                        ->orderBy('history_booking_others.nationality', $data['order'])
                        ->select('history_bookings.*');
                }
            } else {
                if (isset($data['booking_id']) && $data['booking_id'] != '') {
                    $response = $response->join('history_booking_others', 'history_bookings.id', '=', 'history_booking_others.history_booking_id')
                        ->orderBy('history_booking_others.nationality', $data['order'])
                        ->where('history_booking_others.booking_id', $data['booking_id'])
                        ->select('history_bookings.*');
                } else {
                    $response = $response->join('history_booking_others', 'history_bookings.id', '=', 'history_booking_others.history_booking_id')
                        ->orderBy('history_booking_others.nationality', $data['order'])
                        ->select('history_bookings.*');
                }
            }

            if ($data['order_by'] == 'status_name') {
                $response = $response->join('m_booking_status', 'history_bookings.m_booking_status_id', '=', 'm_booking_status.m_booking_status_id')
                        ->orderBy('m_booking_status.name', $data['order'])
                        ->select('history_bookings.*');
            }

            if ($data['order_by'] == 'hotel_name') {
                $response = $response->join('hotel', 'history_bookings.hotel_id', '=', 'hotel.hotel_id')
                        ->orderBy('hotel.name', $data['order'])
                        ->select('history_bookings.*');
            }

            if ($data['order_by'] == 'agent_name') {
                $response = $response->join('agency', 'history_bookings.agency_id', '=', 'agency.agency_id')
                        ->orderBy('agency.name', $data['order'])
                        ->select('history_bookings.*')->get();

                return $response;
            }


            $response = $response->where($data['platform'], $request->user()->platform_id);

//            $response = $this->rest->getModel();

            $response = json_decode($data['pagination']) ? $response->paginate($data['per_page']) : $response = $response->get();

            return $this->listResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }


        return $this->listResponse($response);


    }

    public function show($id) {

        if ($response = $this->rest->getModel()->with(['hasOneHistoryBookingOption','hasOneHistoryBookingInternal', 'hasManyHistoryBookingTax', 'hasOneHistoryBookingOther', 'hasOneHistoryBookingMemo', 'hasManyHistoryBookingRoom', 'hotel', 'tariff', 'agency'])->find($id)) {


            $response->has_one_booking_option = $response->hasOneHistoryBookingOption;
            $response->has_many_booking_tax = $response->hasManyHistoryBookingTax;
            $response->has_one_booking_other = $response->hasOneHistoryBookingOther;
            $response->has_one_booking_memo = $response->hasOneHistoryBookingMemo;
            $response->has_many_booking_room = $response->hasManyHistoryBookingRoom;
            $response->has_one_booking_internal = $response->hasOneHistoryBookingInternal;

            return $this->showResponse($response);
        }

        return $this->notFoundResponse();
    }

    public function store(Request $request) {
        $data = $request->all();

        try {
            $validator = $this->validator($data, $this->rules);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }
            $response = $this->rest->create($data);
            if ($data['has_one_booking_option']) {

                $response->hasOneHistoryBookingOption()->create($data['has_one_booking_option']);
            }
            if ($data['has_one_booking_other']) {
                $response->hasOneHistoryBookingOther()->create($data['has_one_booking_other']);
            }
            if ($data['has_one_booking_internal']) {
                $response->hasOneHistoryBookingInternal()->create($data['has_one_booking_internal']);
            }


            if (isset($data['has_one_booking_memo']) && $data['has_one_booking_memo']) {
                $response->hasOneHistoryBookingMemo()->create($data['has_one_booking_memo']);
            }
            if ($data['has_many_booking_room']) {
                $response->hasManyHistoryBookingRoom()->createMany($data['has_many_booking_room']);
            }
            if ($data['has_many_booking_tax']) {
                $response->hasManyHistoryBookingTax()->createMany($data['has_many_booking_tax']);
            }

            return $this->createdResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }
    }

    public function update($id, Request $request) {

        $data = $request->all();
        $data['data'] = $data;
        if ($response = $this->rest->getModel()->find($id)) {

            try {
                $validator = $this->validator($data, $this->rules);

                if ($validator->fails()) {
                    throw new \Exception("ValidationException");
                }

                $response->deleteRelatedData();
                if (isset($data['has_one_booking_option']) && $data['has_one_booking_option']) {
                    $response->hasOneHistoryBookingOption()->create($data['has_one_booking_option']);
                }
                if (isset($data['has_one_booking_other']) && $data['has_one_booking_other']) {
                    $response->hasOneHistoryBookingOther()->create($data['has_one_booking_other']);
                }
                if (isset($data['has_one_booking_memo']) && $data['has_one_booking_memo']) {
                    $response->hasOneHistoryBookingMemo()->create($data['has_one_booking_memo']);
                }
                if (isset($data['has_many_booking_room']) && $data['has_many_booking_room']) {
                    $response->hasManyHistoryBookingRoom()->createMany($data['has_many_booking_room']);
                }

                $response->fill($data)->save();


                return $this->showResponse($response);
            } catch (\Exception $ex) {
                $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

                return $this->clientErrorResponse($response);
            }
        }

        return $this->notFoundResponse();

    }

    public function destroy($id) {


        if ($response = $this->rest->getModel()->find($id)) {

            $response->deleteRelatedData();
            $response->delete();

            return $this->deletedResponse();

        }

        return $this->notFoundResponse();

    }
}
