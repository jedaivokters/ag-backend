<?php

namespace App\Http\Controllers\V1;


use App\Booking;
use App\Hotel;
use App\Http\Controllers\Controller;
use App\Repositories\Rest\RestRepository;
use Illuminate\Http\Request;

use App\Emails\HotelRegistrationEmail;
use App\Emails\HotelUpdateEmail;


class HotelController extends Controller {


    protected $rules = [
        "hotel_group_id"    => "required",
        "name"              => "required",
        "postal_code"       => "required",
        "prefecture"        => "required",
        "city"              => "required",
        "phone"             => "required",
    ];
    /**
     * @var RestRepository
     */
    private $rest;
    /**
     * @var Booking
     */
    private $booking;

    public function __construct(Hotel $rest, Booking $booking) {

        $this->rest = new RestRepository($rest);
        $this->booking = $booking;
    }


    public function customerOnly($request) {

        $response = [];
        $data = $this->booking->whereAgencyId($request->user()->platform_id)->groupBy('hotel_id')->get();

        foreach ($data as $key => $res) {

            array_push($response, $res->hotel_id);
        }

        return $response;
    }
    public function index(Request $request) {
    	$data = $request->all();

	    try {
		    $validator = $this->validator($data, ['pagination' => 'required']);

		    if ($validator->fails()) {
			    throw new \Exception("ValidationException");
		    }
		    $response = $this->rest->getModel();
		    if(isset($data['name']) && $data['name']){
			    $response = $response->where('name','LIKE','%'.$data['name'].'%');
		    }

            if (isset($data['customer_only']) && $data['customer_only'] && json_decode($data['customer_only'])) {

                $customers = $this->customerOnly($request);
                $response = $response->whereIn('hotel_id', $customers);
            }

		    $response = json_decode($data['pagination']) ? $response->paginate(10) : $response = $response->get();

		    return $this->listResponse($response);
	    } catch (\Exception $ex) {
		    $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

		    return $this->clientErrorResponse($response);
	    }


	    return $this->listResponse($response);



    }

    public function show($id) {
        if ($response = $this->rest->getModel()->find($id)) {
            return $this->showResponse($response);
        }

        return $this->notFoundResponse();
    }

    public function getPlatformByUser(Request $request) {


        $response = $this->rest->getModel()->with(['hasOneUserAccess'])
            ->whereHas('hasOneUserAccess', function ($query) use ($request) {
                $query->whereUserId($request->user()->id);
            })
            ->first();


        if ($response) {
            return $this->showResponse($response);
        }

        return $this->notFoundResponse();
    }

    public function store(Request $request) {
        $data = $request->all();

        try {
            $validator = $this->validator($data, $this->rules);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            } else {
                $response = $this->rest->create($data);

                //TODO: Email status send to logger. Should be on the Email base class
                $registrationEmail = new HotelRegistrationEmail();
                $registrationEmail
                    ->withData(array('name' => $data['name']))
                    ->sendTo(
                        array('email' => $data['account']),
                        array('yes', false)
                    );

                return $this->createdResponse($response);
            }
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }
    }

    public function update($id, Request $request) {

        $data = $request->all();
        if ($response = $this->rest->getModel()->find($id)) {

            try {
                $validator = $this->validator($data, $this->rules);

                if ($validator->fails()) {
                    throw new \Exception("ValidationException");
                }

                $response->fill($data)->save();

                //TODO: Email status send to logger. Should be on the Email base class
                $updateEmail = new HotelUpdateEmail();
                $updateEmail
                    ->withData(array(
                        'sp_name' => $data['name'],
                        'sp_name_jp' => $data['name_zenkaku'],
                        'sp_name_en' => $data['name'],
                        'postal_code' => $data['postal_code'],
                        'prefecture' => $data['prefecture'],
                        'city' => $data['city'],
                        'url' => $data['url'],
                    ))->sendTo(
                        array('email' => $data['account']),
                        array('yes', false)
                    );


                return $this->showResponse($response);
            } catch (\Exception $ex) {
                $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

                return $this->clientErrorResponse($response);
            }
        }

        return $this->notFoundResponse();

    }

    public function destroy($id) {


        if ($response = $this->rest->getModel()->find($id)) {

            $response->delete();

            return $this->deletedResponse();

        }

        return $this->notFoundResponse();

    }
}
