<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\InventoryRoomHeader;
use App\Repositories\Rest\RestRepository;
use Illuminate\Http\Request;


class InventoryRoomController extends Controller {


    protected $rules = [

    ];
    /**
     * @var RestRepository
     */
    private $rest;
    /**
     * @var InventoryRoomHeader
     */
    private $inventoryRoomHeader;

    public function __construct(InventoryRoomHeader $rest,InventoryRoomHeader $inventoryRoomHeader) {

        $this->rest = new RestRepository($rest);
        $this->rest = $rest;
        $this->inventoryRoomHeader = $inventoryRoomHeader;
    }


    public function index(Request $request) {

        $data = $request->all();


        try {
            $validator = $this->validator($data, ['pagination' => 'required']);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }
//            $response = $this->rest->getModel()->whereHotelId($request->user()->platform_id);
            $response = $this->rest->getModel();
            $response = json_decode($data['pagination']) ? $response->paginate(10) : $response = $response->get();

            return $this->listResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }


        return $this->listResponse($response);


    }

    public function show($id,Request $request) {
        $data = $request->all();

        if ($response = $this->rest->getModel()->with(['hasManyInventoryRoomDetail'])->where('month',$id)->whereHotelId($request->user()->platform_id)->first()) {
            return $this->showResponse($response);
        }

        return $this->notFoundResponse();
    }

    public function store(Request $request) {
        $data = $request->all();


        try {
            $validator = $this->validator($data, $this->rules);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }

            $response =  $this->inventoryRoomHeader->updateOrCreate(
                ['hotel_id' => $data['hotel_id'], 'month' => $data['month']],
                $data
            );
            $response->deleteRelatedData();
            $response->hasManyInventoryRoomDetail()->createMany($data['has_many_inventory_room_detail']);

            return $this->createdResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }
    }

    public function update($id, Request $request) {

        $data = $request->all();
        if ($response = $this->rest->getModel()->find($id)) {

            try {
                $validator = $this->validator($data, $this->rules);

                if ($validator->fails()) {
                    throw new \Exception("ValidationException");
                }
                $response->fill($data)->save();


                return $this->showResponse($response);
            } catch (\Exception $ex) {
                $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

                return $this->clientErrorResponse($response);
            }
        }

        return $this->notFoundResponse();

    }

    public function destroy($id) {


        if ($response = $this->rest->getModel()->find($id)) {

//            $response->deleteRelatedData();
            $response->delete();

            return $this->deletedResponse();

        }

        return $this->notFoundResponse();

    }
}
