<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\Traits\RestTrait;
use App\Invitation;
use Illuminate\Http\Request;

use App\Emails\InvitationEmail;



class InvitationController extends Controller {
	use RestTrait;
	const MODEL = 'App\Invitation';
	protected $validationRules = ["hotel_id" => "required"];
	/**
	 * @var User
	 */
	private $invitation;


	public function __construct(Invitation $invitation) {

		$this->invitation = $invitation;
	}

	/**
	 * @param Request $request
	 */
	public function store(Request $request) {

		$data = $request->all();

		try {
			$v = \Validator::make($data, $this->validationRules);

			if ($v->fails()) {
				throw new \Exception("ValidationException");
			}
			
			//TODO: Store invitation record

			//Send invite
			$url = env('FRONTEND_URL', 'http://localhost'). '/agency/reservations/tool/'.$data['hotel_id'];

			$inviteEmal = new InvitationEmail();
			$inviteEmal
				->withData(array(
						'url' => '<a href="'. $url .'">こちらから</a>', // TODO: Should be on a localization
						'sp_name' => $data['hotel_name']
					)
				)->sendTo(
					array('bcc' => $data['invite_email']),
					array('yes', false)
				);

			$response = [
				'code'   => 201,
				'status' => 'succcess',
				'data'   => $data,
			];

			return response()->json($response, 200);


		} catch (\Exception $ex) {

			$error = ['form_validations' => $v->errors(), 'exception' => $ex->getMessage()];

			$response = [
				'code'    => 422,
				'status'  => 'error',
				'data'    => $error,
				'message' => 'Unprocessable entity',
			];

			return response()->json($response, $response['code']);

		}


	}

}
