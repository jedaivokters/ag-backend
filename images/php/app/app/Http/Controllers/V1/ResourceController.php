<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\MBookingStatus;
use App\Repositories\Rest\RestRepository;
use Illuminate\Http\Request;

use App\Emails\InquiryEmail;


class ResourceController extends Controller {


    /**
     * @var MBookingStatus
     */
    private $mBookingStatus;

    public function __construct(MBookingStatus $mBookingStatus) {

        $this->mBookingStatus = $mBookingStatus;
    }


    public function getBookingStatuses() {

        $response = $this->mBookingStatus->get();

        return $this->showResponse($response);
    }


    public function inquiry(Request $request) {

        $data = $request->all();

        $platform = ($data['platform'] == 'sp') ? 'Hotel' : 'Agency';

        //TODO: Email status send to logger. Should be on the Email base class
        $inquiry = new InquiryEmail();
        $inquiry
            ->withData(array(
                'name' => $data['name'],
                'platform' => $platform,
                'message' => $data['message'],
                'subject' => $data['subject'],
            ))->sendTo(
                array('email' => 'support_agthotels@acomo-inc.co.jp'),
                array('yes', false)
            );


        return $this->showResponse($data);
    }


}
