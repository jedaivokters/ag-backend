<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\Repositories\Rest\RestRepository;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RoomController extends Controller {


    protected $rules = [
        "hotel_id"        => "required",
        "room_type_id"    => "required",
        "bed_type_id"     => "required",
        "pax_with_bed"    => "required",
        "pax_without_bed" => "required",
        "name"            => "required",
        "count"           => "required",
    ];
    /**
     * @var RestRepository
     */
    private $rest;

    public function __construct(Room $rest) {

        $this->rest = new RestRepository($rest);
    }


    public function index(Request $request) {

        $data = $request->all();


        try {
            $validator = $this->validator($data, ['pagination' => 'required']);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }
            $response = $this->rest->getModel()->whereHotelId($request->user()->platform_id);

            if (isset($data['with_stock']) && $data['with_stock']) {
                $response = $response->with([
                    'hasManyBookingRoom' => function ($query) use ($data, $request) {

                        $query->with(['booking']);
                        if (isset($data['month']) && $data['month']) {
                            $date = explode('-', $data['month']);
                            $query->whereYear('date', $date[0])
                                ->whereMonth('date', $date[1]);
                        }
                        $query->whereHas('booking', function ($query) use ($request) {
                            $query->whereHotelId($request->user()->platform_id)
                                ->whereIn('m_booking_status_id',[2,3,4,5]);
                        });

                    },
                ]);
            }
//            $response = $this->rest->getModel();
            $response = json_decode($data['pagination']) ? $response->paginate(10) : $response = $response->get();


            foreach ($response as $key => $parent) {
                $rooms = $this->getReserveRoom($parent->hasManyBookingRoom);

                unset($parent->hasManyBookingRoom);

                $response[ $key ]['has_many_reserved_room'] = $rooms;
            }

            return $this->listResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }


        return $this->listResponse($response);


    }

    public function getReserveRoom($rooms) {

        $result = [];
        foreach ($rooms as $row) {
            if (!isset($result[ $row['date'] ])) {
                $result[ $row['date'] ] = $row;
            } else {
                $result[ $row['date'] ]['quantity'] += $row['quantity'];
            }
        }


        $reserved = [];
        foreach ($result as $row) {
            $input = [
                "room_id"  => $row['room_id'],
                "quantity" => $row['quantity'],
                "date"     => $row['date'],
            ];

            array_push($reserved, $input);
        }

        return $reserved;
    }

    public function show($id) {
        if ($response = $this->rest->getModel()->with(['hasManyRealRoom', 'hasManyRoomEquipment'])->find($id)) {
            return $this->showResponse($response);
        }

        return $this->notFoundResponse();
    }

    public function store(Request $request) {
        $data = $request->all();

        try {
            $validator = $this->validator($data, $this->rules);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }
            $response = $this->rest->create($data);
            $response->hasManyRealRoom()->createMany($data['has_many_real_room']);
            $response->hasManyRoomEquipment()->createMany($data['has_many_room_equipment']);

            return $this->createdResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }
    }

    public function update($id, Request $request) {

        $data = $request->all();
        if ($response = $this->rest->getModel()->find($id)) {

            try {
                $validator = $this->validator($data, $this->rules);

                if ($validator->fails()) {
                    throw new \Exception("ValidationException");
                }
                $response->fill($data)->save();
                $response->deleteRelatedData();

                $response->hasManyRealRoom()->createMany($data['has_many_real_room']);
                $response->hasManyRoomEquipment()->createMany($data['has_many_room_equipment']);

                return $this->showResponse($response);
            } catch (\Exception $ex) {
                $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

                return $this->clientErrorResponse($response);
            }
        }

        return $this->notFoundResponse();

    }

    public function destroy($id) {


        if ($response = $this->rest->getModel()->find($id)) {

            $response->deleteRelatedData();
            $response->delete();

            return $this->deletedResponse();

        }

        return $this->notFoundResponse();

    }
}
