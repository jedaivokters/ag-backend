<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\Traits\RestTrait;


class RoomTypeController extends Controller {
	use RestTrait;
	const MODEL = 'App\RoomType';
	protected $validationRules = [

		"name",

	];


}
