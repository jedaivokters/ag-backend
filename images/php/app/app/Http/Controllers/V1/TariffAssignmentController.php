<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\Repositories\Rest\RestRepository;
use App\TariffAssignment;
use Illuminate\Http\Request;


class TariffAssignmentController extends Controller {


    protected $rules = [

    ];
    /**
     * @var RestRepository
     */
    private $rest;

    public function __construct(TariffAssignment $rest) {

        $this->rest = new RestRepository($rest);
    }


    public function index(Request $request) {

        $data = $request->all();

        try {
            $validator = $this->validator($data, []);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }
            $response = $this->rest->getModel()->with(['agency']);

            $response = $response->whereTariffId($data['tariff_id'])->get();

            return $this->listResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }


        return $this->listResponse($response);


    }

    public function show($id) {
        
    }

    public function store(Request $request) {
        $data = $request->all();

        try {
            $validator = $this->validator($data, ['tariff_id']);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }

            //Remove all? TODO: Remove only unselected agency
            $this->rest->getModel()
                ->where('tariff_id', $data['tariff_id'])
                ->delete();

            $assigned = array_map(function ($data) {
                unset($data['name']);
                if (isset($data['agency'])) {
                    unset($data['agency']);
                }
                if (isset($data['tariff_assignment_id'])) {
                    unset($data['tariff_assignment_id']);
                }
                return $data;
            }, $data['assigned']);

            $response = $this->rest->getModel()->insert($assigned);

            // $response = call_user_func_array([$this->rest->getModel(), 'firstOrCreate'], $assigned);

            return $this->createdResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }
    }

    public function update($id, Request $request) {

    }

    public function destroy($id) {
        
    }
}
