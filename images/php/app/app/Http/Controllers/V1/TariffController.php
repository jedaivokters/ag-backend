<?php

namespace App\Http\Controllers\V1;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Repositories\Rest\RestRepository;
use App\Tariff;


class TariffController extends Controller {


    protected $rules = [

    ];
    /**
     * @var RestRepository
     */
    private $rest;

    public function __construct(Tariff $rest) {

        $this->rest = new RestRepository($rest);
    }


    public function index(Request $request) {

        $data = $request->all();

        $data['hotel_id'] = $request->user()->platform == 'sp' ? $request->user()->platform_id : $data['hotel_id'];

//		$data['hotel_id'] = $request->user()->platform_id;

        try {
            $validator = $this->validator($data, ['pagination' => 'required', 'hotel_id' => 'required']);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }
            $response = $this->rest->getModel()->with(['hasOneTariffSettingOption'])->whereActive(1);

            // TODO: Search filter must be refactor into filter class : https://github.com/mohammad-fouladgar/eloquent-builder
            if ($request->has('on_tool')) { // Condition on OP reservation only
                $response->select('tariff.*');
                $response->leftJoin('tariff_assignment', function ($join) use ($data) {
                    $join->on('tariff_assignment.tariff_id', '=', 'tariff.tariff_id');
                    $join->on('tariff_assignment.op_id', '=', DB::raw($data['agency_id']));
                })
                    ->where('readiness', 'Open')
                    ->whereRaw('(tariff_assignment.tariff_id IS NOT NULL OR publicity = "All Connected")');
            }

            if ($request->has('on_limited_setting')) {
                $response->where('publicity', 'Only Limited');
            }

            $response = $response->whereHotelId($data['hotel_id']);
            // echo $response->toSql(); // TO DEBUG

            $response = json_decode($data['pagination']) ? $response->paginate(10) : $response = $response->get();

            return $this->listResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }


        return $this->listResponse($response);


    }

    public function show($id) {
        if ($response = $this->rest->getModel()->with(['hasOneTariffSettingOption', 'hasManyRoomSeasonalityPrice'])->find($id)) {
            return $this->showResponse($response);
        }

        return $this->notFoundResponse();
    }

    public function store(Request $request) {
        $data = $request->all();

        $data['data'] = isset($data['data']) && $data['data'] ? $data['data'] : $data;

        try {
            $validator = $this->validator($data, $this->rules);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }
            $response = $this->rest->create($data);
            $response->hasOneTariffSettingOption()->create($data['has_one_tariff_setting_option']);
            $response->hasManyRoomSeasonalityPrice()->createMany($data['has_many_room_seasonality_price']);


            return $this->createdResponse($response);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }
    }

    public function update($id, Request $request) {

        $data = $request->all();
        $data['data'] = $data;
        if ($response = $this->rest->getModel()->find($id)) {

            try {
                $validator = $this->validator($data, $this->rules);

                if ($validator->fails()) {
                    throw new \Exception("ValidationException");
                }

                $response->deleteRelatedData();
                if (isset($data['has_one_tariff_setting_option']) && $data['has_one_tariff_setting_option']) {
                    $response->hasOneTariffSettingOption()->create($data['has_one_tariff_setting_option']);
                }

                if (isset($data['has_many_room_seasonality_price']) && $data['has_many_room_seasonality_price']) {
                    $response->hasManyRoomSeasonalityPrice()->createMany($data['has_many_room_seasonality_price']);
                }

                $response->fill($data)->save();


                return $this->showResponse($response);
            } catch (\Exception $ex) {
                $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

                return $this->clientErrorResponse($response);
            }
        }

        return $this->notFoundResponse();

    }

    public function destroy($id) {


        if ($response = $this->rest->getModel()->find($id)) {

            $response->active = 0;
            $response->save();

            return $this->deletedResponse();

        }

        return $this->notFoundResponse();

    }
}
