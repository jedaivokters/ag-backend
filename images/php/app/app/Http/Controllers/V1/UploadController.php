<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class UploadController extends Controller {

    //

    protected $rules = [
        "file" => 'required|file|max:3000',

    ];

    protected $uploadPath;

    public function __construct() {

        $this->uploadPath = "uploads/";
    }


    public function file(Request $request) {
        $data = $request->all();


        try {
            $validator = $this->validator($data, $this->rules);

            if ($validator->fails()) {
                throw new \Exception("ValidationException");
            }

            if (!File::exists($this->uploadPath)) {
                File::makeDirectory($this->uploadPath);
            }

            $file = Input::file('file');

            $name = $file->getClientOriginalName();
            $fileName = time() . $name;

            $file->move($this->uploadPath, $fileName);

            $res = [
                'file_name'     => $fileName,
                'path'          => url($this->uploadPath) . '/',
                'original_name' => $name,
            ];

            return $this->createdResponse($res);
        } catch (\Exception $ex) {
            $response = ['form_validations' => $validator->errors(), 'exception' => $ex->getMessage()];

            return $this->clientErrorResponse($response);
        }


    }


}
