<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class InventoryRoomDetail extends Model {

	protected $fillable = [
        "inventory_room_header_id",
        "room_id",
        "note",
        "regular_quantity",
        "max_quantity",
        "original_quantity",
        "date"
    ];

}
