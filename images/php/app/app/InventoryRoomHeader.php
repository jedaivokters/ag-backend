<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class InventoryRoomHeader extends Model {


    protected $casts = [
        'setting' => 'array',
    ];

	protected $fillable = [
        "hotel_id",
        "setting",
        "month",
        "created_at",
        "updated_at"
    ];


	public function hasManyInventoryRoomDetail() {
	    return $this->hasMany('App\InventoryRoomDetail');
	}

    public function deleteRelatedData(){

        $this->hasManyInventoryRoomDetail()->delete();
    }
}
