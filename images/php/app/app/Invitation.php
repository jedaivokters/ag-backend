<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Invitation extends Model {

    protected $table = 'invitation';
    protected $primaryKey = 'invitation_id';
    protected $fillable = [
        "hotel_id",
        "agency_id",
        "invite_email",
        "status_code",
    ];
}
