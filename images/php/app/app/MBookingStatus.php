<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class MBookingStatus extends Model {
	protected $table = 'm_booking_status';
	protected $primaryKey = 'm_booking_status_id';

	protected $fillable = [];
}
