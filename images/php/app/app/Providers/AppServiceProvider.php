<?php

namespace App\Providers;

use App\Repositories\Rest\Contracts\RestInterface;
use App\Repositories\Rest\RestRepository;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{


	public function boot()
	{
		//



        Relation::morphMap([
            'hotel' => \App\Hotel::class,
            'agency' => \App\Agency::class,
       ]);

		Schema::defaultStringLength(191);
	}

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(RestInterface::class, RestRepository::class);

    }
}
