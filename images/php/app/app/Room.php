<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Room extends Model {
	protected $table = 'room';
	protected $primaryKey = 'room_id';

	protected $fillable = [
		"hotel_id",
		"room_type_id",
		"bed_type_id",
		"pax_with_bed",
		"pax_without_bed",
		"smoking_enable_flg",
		"size",
		"name",
		"count",
	];


	public function hasManyRealRoom() {
	    return $this->hasMany('App\RealRoom','room_id');
	}

	public function hasManyBookingRoom() {
	    return $this->hasMany('App\BookingRoom','room_id');
	}
	public function hasManyRoomEquipment() {
	    return $this->hasMany('App\RoomEquipment','room_id');
	}

	public function deleteRelatedData(){

	    $this->hasManyRealRoom()->delete();
	    $this->hasManyRoomEquipment()->delete();
    }
}
