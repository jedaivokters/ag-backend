<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class RoomSeasonalityPrice extends Model {
	protected $table = 'room_seasonality_price';
	protected $primaryKey = 'room_seasonality_price_id';

	protected $fillable = [
        "label" , "price", "room_id", "tariff_id"
	];


}
