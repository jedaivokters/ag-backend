<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Tariff extends Model {
    protected $table = 'tariff';
    protected $primaryKey = 'tariff_id';

    protected $casts = [
        'data' => 'array',
        'room_settings' => 'array',
    ];

    protected $fillable =[
        "hotel_id",
        "name",
        "tariff_number",
        "pax_min",
        "pax_max",
        "start_date",
        "end_date",
        "tax_inclusion",
        "vat_inclusion",
        "other_inclusion",
        "tax_other",
        "checkin_time",
        "checkout_time",
        "conductor_free_count",
        "commission_info",
        "general_info",
        "cancel_policy",
        "status_code",
        "payment_information",
        "restaurant_time",
        "smoking_policy",
        "cut_off_dates",
        "publicity",
        "readiness",
        "organization",
        "night_min",
        "night_max",
        "data",
        "room_settings",
        "active"
    ];


    protected $appends = ['id'];
    public function hasManyRoomSeasonalityPrice() {

        return $this->hasMany('App\RoomSeasonalityPrice', 'tariff_id');
    }

    public function hasOneTariffSettingOption(){
        return $this->hasOne('App\TariffSettingOption','tariff_id');
    }

    public function deleteRelatedData() {
        $this->hasManyRoomSeasonalityPrice()->delete();
        $this->hasOneTariffSettingOption()->delete();
    }

    public function getIdAttribute() {


        return $this->tariff_id;
    }



}
