<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class TariffAssignment extends Model {
    protected $table = 'tariff_assignment';
    protected $primaryKey = 'tariff_assignment_id';
    public $timestamps = false; 

    protected $fillable =[
        "tariff_id",
        "op_id",
    ];

    public function agency() {
        return $this->belongsTo('App\Agency','op_id');
    }

}
