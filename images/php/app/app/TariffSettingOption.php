<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class TariffSettingOption extends Model {

    protected $casts = [
        'children_no_beds' => 'array',
//        'driver_guides' => 'array',
        'meals' => 'array',
        'others' => 'array',
    ];


    protected $fillable = [
        'children_no_beds',
        'tariff_id',
//        'driver_guides',
        'meals',
        'tariff_id',
        'others',
    ];


}
