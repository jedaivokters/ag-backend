<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements JWTSubject, AuthenticatableContract, AuthorizableContract {
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $appends = ['platform_id', 'platform'];


    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }

    public function getPlatform() {

        $response = \App\UserAccess::whereUserId($this->getJWTIdentifier())->first();

        return $response;
    }

    public function setPasswordAttribute($pass) {

        $this->attributes['password'] = Hash::make($pass);

    }


    public function hasManyUserAccess() {
        return $this->hasMany('App\UserAccess');
    }

    public function hasOneUserAccess() {
        return $this->hasOne('App\UserAccess');
    }

    public function getPlatformIdAttribute() {


        return isset($this->getPlatform()->platform_id) ? $this->getPlatform()->platform_id : 0;
    }


    public function getPlatformAttribute() {


        return isset($this->getPlatform()->type) ? $this->getPlatform()->type : '';
    }
}
