<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class VacancyType extends Model {
	protected $table = 'vacancy_type';
	protected $primaryKey = 'vacancy_type_id';

	protected $fillable = [];
}