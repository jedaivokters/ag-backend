<?php
return [
    /*
    |--------------------------------------------------------------------------
    | JWT Configuration
    |--------------------------------------------------------------------------
    |
    | Set expiration and etc.
    |
    */
    'ttl' => env('JWT_TTL', 1440) //defualt 24 hrs
];