<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpInformationMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('op_information_member', function (Blueprint $table) {
            
            $table->bigIncrements('op_information_member_id');
            $table->smallInteger('agency_id');
            $table->char('name', 255);
            $table->char('email', 255); 
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('op_information_member');
    }
}
