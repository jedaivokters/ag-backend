<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionSeasonalityPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('option_seasonality_price', function (Blueprint $table) {
            //
            $table->bigIncrements('option_seasonality_price_id');
            $table->smallInteger('tariff_id');
            $table->smallInteger('room_id');
            $table->smallInteger('seasonality_id');
            $table->char('label', 255);
            $table->smallInteger('price');
            $table->smallInteger('view_order');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('option_seasonality_price');
    }
}
