<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacancyTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancy_type', function (Blueprint $table) {
            //
            $table->bigIncrements('vacancy_type_id');
            $table->char('name', 255);
            $table->char('label', 255);
            $table->smallInteger('vacancy_number');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vacancy_type');
    }
}
