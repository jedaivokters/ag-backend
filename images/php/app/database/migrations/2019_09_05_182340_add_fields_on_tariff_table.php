<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsOnTariffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tariff', function (Blueprint $table) {
            //
            $table->longText('payment_information');
            $table->longText('restaurant_time');
            $table->longText('cut_off_dates');
            $table->string('publicity');
            $table->string('readiness');
            $table->string('organization');
            $table->integer('night_min');
            $table->integer('night_max');
            $table->longText('data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tariff', function (Blueprint $table) {
            //

            $table->dropColumn('payment_information');
            $table->dropColumn('restaurant_time');
            $table->dropColumn('cut_off_dates');
            $table->dropColumn('publicity');
            $table->dropColumn('readiness');
            $table->dropColumn('organization');
            $table->dropColumn('night_min');
            $table->dropColumn('data');

        });
    }
}
