<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTariffSettingOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('tariff_setting_options', function (Blueprint $table) {
            //

            $table->engine = "MyISAM";
            $table->integer('tariff_id')->unsigned();
            $table->foreign('tariff_id')
                ->references('id')
                ->on('tariff')
                ->onDelete('cascade');
            $table->longText('children_no_beds');
            $table->longText('driver_guides');
            $table->longText('meals');
            $table->longText('others');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tariff_setting_options');
    }
}
