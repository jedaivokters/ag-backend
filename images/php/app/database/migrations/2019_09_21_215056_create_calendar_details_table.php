<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_details', function (Blueprint $table) {
            $table->engine = "MyISAM";
            $table->bigIncrements('id');


            $table->integer('calendar_header_id')->unsigned();
            $table->foreign('calendar_header_id')
                ->references('id')
                ->on('calendar_headers')
                ->onDelete('cascade');

            $table->string('month');
            $table->longText('days');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar_details');
    }
}
