<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCalendarPublicityFieldHotelTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('hotel', function (Blueprint $table) {
            //
            $table->integer('calendar_publicity')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('hotel', function (Blueprint $table) {
            //
            $table->dropColumn('calendar_publicity');

        });
    }
}
