<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsBookingOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_option', function (Blueprint $table) {
            //

            $table->integer('booking_id');
            $table->longText('children_no_beds');
            $table->longText('driver_guides');
            $table->longText('meals');
            $table->longText('others');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_option', function (Blueprint $table) {
            //
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('children_no_beds');
            $table->dropColumn('driver_guides');
            $table->dropColumn('meals');
            $table->dropColumn('others');
        });
    }
}
