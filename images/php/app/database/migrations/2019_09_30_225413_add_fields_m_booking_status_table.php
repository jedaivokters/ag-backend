<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsMBookingStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_booking_status', function (Blueprint $table) {
            //
            $table->timestamps();
            $table->string('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_booking_status', function (Blueprint $table) {
            //


            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('name');
        });
    }
}
