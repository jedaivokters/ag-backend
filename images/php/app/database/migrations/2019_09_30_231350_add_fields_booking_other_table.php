<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsBookingOtherTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('booking_other', function (Blueprint $table) {
            //

            $table->integer('booking_id');
            $table->string('nationality');
            $table->string('tour_code');
            $table->string('group_name');
            $table->string('transportation');
            $table->string('arrival_time');
            $table->string('departure_time');
            $table->longText('message_hotel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('booking_other', function (Blueprint $table) {
            //

            $table->dropColumn('booking_id');
            $table->dropColumn('nationality');
            $table->dropColumn('tour_code');
            $table->dropColumn('group_name');
            $table->dropColumn('transportation');
            $table->dropColumn('arrival_time');
            $table->dropColumn('departure_time');
            $table->dropColumn('message_hotel');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
}
