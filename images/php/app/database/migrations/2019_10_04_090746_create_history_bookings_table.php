<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_bookings', function (Blueprint $table) {
            $table->engine = "MyISAM";
            $table->bigIncrements('id');
            $table->integer('booking_id');
            $table->smallInteger('version');
            $table->smallInteger('tariff_id');
            $table->smallInteger('agency_id');
            $table->smallInteger('hotel_id');
            $table->smallInteger('m_booking_status_id');
            $table->char('booking_date', 255);
            $table->date('check_in_date');
            $table->date('check_out_date');
            $table->smallInteger('number_nights');
            $table->smallInteger('number_pax');
            $table->date('inquiry_date');
            $table->date('booked_date');
            $table->date('confirmed_date');
            $table->date('finalized_date');
            $table->char('booking_remarks', 255);
            $table->smallInteger('booking_reminder_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_bookings');
    }
}
