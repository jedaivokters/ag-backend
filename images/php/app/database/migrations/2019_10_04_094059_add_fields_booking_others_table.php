<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsBookingOthersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_other', function (Blueprint $table) {
            //
            $table->string('contact_conductor_number');
            $table->string('contact_conductor_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_other', function (Blueprint $table) {
            //
            $table->dropColumn('contact_conductor_number');
            $table->dropColumn('contact_conductor_name');
        });
    }
}
