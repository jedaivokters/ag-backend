<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryBookingOthersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_booking_others', function (Blueprint $table) {
            $table->engine = "MyISAM";
            $table->bigIncrements('id');
            $table->integer('booking_id');
            $table->integer('booking_other_id');
            $table->string('nationality');
            $table->string('tour_code');
            $table->string('group_name');
            $table->string('transportation');
            $table->string('arrival_time');
            $table->string('departure_time');
            $table->longText('message_hotel');
            $table->string('contact_conductor_number');
            $table->string('contact_conductor_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_booking_others');
    }
}
