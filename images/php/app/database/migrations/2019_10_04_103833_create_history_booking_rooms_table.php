<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryBookingRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_booking_rooms', function (Blueprint $table) {

            $table->engine = "MyISAM";
            $table->bigIncrements('id');
            $table->integer('booking_room_id');
            $table->date('date');
            $table->integer('room_id');

            $table->string('name');
            $table->integer('number');
            $table->double('price');
            $table->double('total');
            $table->string('vat');
            $table->integer('quantity');
            $table->integer('night');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_booking_rooms');
    }
}
