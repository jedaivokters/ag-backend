<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryBookingMemosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_booking_memos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('booking_memo_id');
            $table->smallInteger('booking_id');
            $table->char('category', 255);
            $table->smallInteger('memo_type');
            $table->smallInteger('probability');
            $table->char('payment', 255);
            $table->char('namelist', 255);
            $table->longText('note');
            $table->smallInteger('booking_probability_type_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_booking_memos');
    }
}
