<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsHistoryBookingIdHistoryBookingOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_booking_options', function (Blueprint $table) {
            //
            $table->integer('history_booking_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_booking_options', function (Blueprint $table) {
            //
            $table->dropColumn('history_booking_id');
        });
    }
}
