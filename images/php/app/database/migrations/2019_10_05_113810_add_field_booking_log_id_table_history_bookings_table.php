<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldBookingLogIdTableHistoryBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_bookings', function (Blueprint $table) {
            //
            $table->integer('booking_log_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_bookings', function (Blueprint $table) {
            //
            $table->dropColumn('booking_log_id');
        });
    }
}
