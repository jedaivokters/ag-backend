<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsBookingTaxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_tax', function (Blueprint $table) {
            //
            $table->integer('booking_id');
            $table->string('name');
            $table->integer('quantity');
            $table->string('night');
            $table->double('price');
            $table->double('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_tax', function (Blueprint $table) {
            //
            $table->dropColumn('booking_id');
            $table->dropColumn('name');
            $table->dropColumn('quantity');
            $table->dropColumn('night');
            $table->dropColumn('price');
            $table->dropColumn('total');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
}
