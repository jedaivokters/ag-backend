<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsTotalColumnsBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking', function (Blueprint $table) {
            //
            $table->double('room_total')->after('total');
            $table->double('option_total')->after('total');
            $table->double('tax_total')->after('total');
            $table->double('commission_total')->after('total');
            $table->double('sub_total')->after('total');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking', function (Blueprint $table) {
            //

            $table->dropColumn('room_total');
            $table->dropColumn('option_total');
            $table->dropColumn('tax_total');
            $table->dropColumn('commission_total');
            $table->dropColumn('sub_total');


        });
    }
}
