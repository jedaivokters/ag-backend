<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsTotalColumnsHistoryBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_bookings', function (Blueprint $table) {
            //
            $table->double('room_total');
            $table->double('option_total');
            $table->double('tax_total');
            $table->double('commission_total');
            $table->double('sub_total');
            $table->double('grand_total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_bookings', function (Blueprint $table) {
            //
            $table->dropColumn('room_total');
            $table->dropColumn('option_total');
            $table->dropColumn('tax_total');
            $table->dropColumn('commission_total');
            $table->dropColumn('sub_total');
            $table->dropColumn('grand_total');
        });
    }
}
