<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusDateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking', function (Blueprint $table) {
            //
            $table->date('canceled_date')->after('finalized_date');
            $table->date('refused_date')->after('finalized_date');
            $table->date('completed_date')->after('finalized_date');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking', function (Blueprint $table) {
            //
            $table->dropColumn('completed_date');
            $table->dropColumn('refused_date');
            $table->dropColumn('canceled_date');

        });
    }
}
