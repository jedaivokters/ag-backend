<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileFieldsBookingMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_messages', function (Blueprint $table) {
            //
            $table->integer('has_file');
            $table->string('original_name');
            $table->string('file_name');
            $table->string('path');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_messages', function (Blueprint $table) {
            //
            $table->dropColumn('has_file');
            $table->dropColumn('original_name');
            $table->dropColumn('file_name');
            $table->dropColumn('path');
        });
    }
}
