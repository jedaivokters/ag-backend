<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOriginQuantityFieldInventoryRoomDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory_room_details', function (Blueprint $table) {
            //
            $table->integer('original_quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_room_details', function (Blueprint $table) {
            //
            $table->dropColumn('original_quantity');
        });
    }
}
