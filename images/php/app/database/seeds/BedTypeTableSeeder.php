<?php

use App\BedType;
use Illuminate\Database\Seeder;

class BedTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        BedType::truncate();
        BedType::insert([

            [

                "name" => "1 Twin Bed"
            ],
            [

                "name" => "2 Twin Beds"
            ],
            [

                "name" => "1 Queen Bed "
            ],
            [

                "name" => "1 King Bed "
            ],
            [

                "name" => "2 Large Twin Beds"
            ],
            [

                "name" => "2 Queen Beds"
            ],
            [

                "name" => "2 King Beds"
            ],
            [

                "name" => "2 Twin Beds + 1 Twin Sofa Bed "
            ],
            [

                "name" => "2 Twin Beds + 1 Double Sofa Bed"
            ],
            [

                "name" => "2 Futon Beds"
            ],
            [

                "name" => "2 Futon Beds and 2 Twin Beds"
            ],
        ]);
    }
}
