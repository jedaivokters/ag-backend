<?php

use App\MBookingStatus;
use Illuminate\Database\Seeder;

class MBookingStatusTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //


        MBookingStatus::truncate();


        MBookingStatus::insert([

            [
                "m_booking_status_id" => 1,
                "name" => "Requested"
            ],
            [
                "m_booking_status_id" => 2,
                "name" => "Reserved"
            ],
            [
                "m_booking_status_id" => 3,
                "name" => "Confirmed"
            ],
            [
                "m_booking_status_id" => 4,
                "name" => "Finalized"
            ],
            [
                "m_booking_status_id" => 5,
                "name" => "Completed"
            ],
            [
                "m_booking_status_id" => 6,
                "name" => "Cancel"
            ],
            [
                "m_booking_status_id" => 7,
                "name" => "Refuse"
            ]
        ]);

    }
}
