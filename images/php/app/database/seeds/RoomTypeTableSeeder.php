<?php

use App\RoomType;
use Illuminate\Database\Seeder;

class RoomTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        RoomType::truncate();
        RoomType::insert([

            [

                "name" => "SGL Room"
            ],
            [

                "name" => "TWN Room"
            ],
            [

                "name" => "DBL Room"
            ],
            [

                "name" => "TRP Room"
            ],
            [

                "name" => "Quad Room"
            ],
            [

                "name" => "Family Room"
            ],
            [

                "name" => "Japanese Room"
            ]
        ]);
    }
}
