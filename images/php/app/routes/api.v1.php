<?php

function rest($router, $path, $controller) {


    $router->get($path, $controller . '@index');
    $router->get($path . '/{id}', $controller . '@show');
    $router->post($path, $controller . '@store');
    $router->put($path . '/{id}', $controller . '@update');
    $router->delete($path . '/{id}', $controller . '@destroy');
}

$router->group(['middleware' => 'auth:api'], function ($router) {

    $router->group(['prefix' => "hotels"], function ($router) {
        $router->get('get-platform-by-user', 'HotelController@getPlatformByUser');
    });

    $router->group(['prefix' => "agencies"], function ($router) {
        $router->get('get-platform-by-user', 'AgencyController@getPlatformByUser');
    });

    rest($router, '/rooms', 'RoomController');
    rest($router, '/user-accesses', 'UserAccessController');
    rest($router, '/person-in-charges', 'PersonInChargeController');
    rest($router, '/bed-types', 'BedTypeController');
    rest($router, '/real-rooms', 'RealRoomController');
    rest($router, '/room-equipments', 'RoomEquipmentController');
    rest($router, '/room-types', 'RoomTypeController');
    rest($router, '/vacancy-types', 'VacancyTypeController');
    rest($router, '/vacancy-states', 'VacancyStateController');
    rest($router, '/tariffs', 'TariffController');
    rest($router, '/calendars', 'CalendarController');
    rest($router, '/control-settings', 'ControlSettingController');

    rest($router, '/bookings', 'BookingController');
    rest($router, '/booking-messages', 'BookingMessageController');
    rest($router, '/booking-other-infos', 'BookingOtherInfoController');
    rest($router, '/booking-options', 'BookingOptionController');
    rest($router, '/booking-rooms', 'BookingRoomController');
    rest($router, '/booking-memos', 'BookingMemoController');
    rest($router, '/booking-logs', 'BookingLogController');
    rest($router, '/booking-taxes', 'BookingTaxController');
    rest($router, '/booking-internal-infos', 'BookingInternalInfoController');

	rest($router, '/booking-histories', 'HistoryBookingController');

    rest($router, '/invitations', 'InvitationController');
    rest($router, '/inventory-rooms', 'InventoryRoomController');
	$router->group(['prefix' => "resources"], function($router){

	    $router->get('booking-statuses', 'ResourceController@getBookingStatuses');
        $router->post('inquiry', 'ResourceController@inquiry');
	});
    rest($router, '/tariff-assignment', 'TariffAssignmentController');

});
rest($router, '/agencies', 'AgencyController');
rest($router, '/hotels', 'HotelController');
rest($router, '/users', 'UserController');
$router->group(['prefix' => "users"], function ($router) {
    $router->post('request-for-account', 'UserController@requestForAccount');
});

$router->group(['prefix' => "upload"], function($router){
    $router->post('file','UploadController@file');
});

$router->group(['prefix' => "auth"], function ($router) {

    $router->post('/login', 'AuthController@login');
    $router->get('/test', 'AuthController@test');
    $router->post('/change-password', 'AuthController@changePassword');
    $router->post('/logout', 'AuthController@logout');
    $router->get('/profile', 'AuthController@profile');


});
